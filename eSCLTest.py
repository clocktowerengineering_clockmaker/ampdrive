import socket, time

DriveIP = "192.168.0.140" #Drive IP can be configured

#header and end required for each eSCL message
header = [0x00, 0x07]
end = 0xD

UDP_PORT = 7775

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#Binding to your computer or external device running Python will vary.
sock.bind(('192.168.0.100', 7774)) #Match to your device

# This function encodes the message attaches header and adds carriage return to
# the end. It also recieves the drive response and prints it.
def command(Message):
    toSend = bytearray()
    toSend.extend(header)
    toSend.extend(Message)
    toSend.append(end)
    print toSend
    #print len(toSend)
    sock.sendto(toSend,(DriveIP,UDP_PORT))
    #print "Sent"
    recMessage = sock.recv(1024).decode()
    print len(recMessage)
    print recMessage.index('=')
    print recMessage[recMessage.index('=')+1:]
    print(recMessage)
    
# Drives should jog for 5 seconds in each direction        
command('SC')    
command('SP')    
time.sleep(.1)
command('DI1')    
time.sleep(.1)
command('JS15.5')
time.sleep(.1)
command('CJ')
time.sleep(5)
command('SJ')
time.sleep(.1)
command('DI-1')
time.sleep(.1)
command('CJ')
time.sleep(5)
command('SJ')
